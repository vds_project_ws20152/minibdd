#ifndef MANAGER_HPP_
#define MANAGER_HPP_

#include <iostream>
#include <unordered_map>
#include <set>
#include <exception>
#include <stdexcept>
#include <sstream>

using namespace std;

/**
 * @brief Exception for invalid BDD_ID
 * @details Throw this exception when an invalid BDD_ID is inputted to APIs
 */
class Invalid_BDD_ID_Exeption: public exception {
	virtual const char* what() const throw()
	  {
	    return "Invalid BDD_ID Exception";
	  }
};

typedef size_t BDD_ID;

/**
 * @brief ITE structure
 * @details Structure to store information about an ITE operator, include its IF, THEN, and ELSE function branches
 */
 struct ITE {
 	/**
 	 * BDD ID of the IF branch
 	 */
	BDD_ID i;

	/**
	 * BDD ID of the THEN branch
	 */
	BDD_ID t;

	/**
	 * BDD ID of the ELSE branch
	 */
	BDD_ID e;

	/**
	 * @brief Operator Overloading for Equal Operator
	 * @details Equal operator overloading to compare two ITE operators
	 *
	 * @param otherITE the second rvalue ITE structure
	 * @return true if equal, otherwise false
	 */
	bool operator == (const ITE &otherITE) const {
		return (i == otherITE.i
				&& t == otherITE.t
				&& e == otherITE.e);
	}

};

namespace std {

	/**
	 * Overiding hash function for ITE structure
	 */
  template <>
  struct hash<ITE>
  {
  	/**
	 	 * @brief Hashing function for an ITE structure
	 	 * @details Override hashing function for an ITE structure
	 	 *
	 	 * @param k The ITE need to be hashed
	 	 * @return index for that ITE
	 	 */
	  BDD_ID operator()(const ITE& k) const
    {
      // Compute individual hash values for first,
      // second and third and combine them using XOR
      // and bit shifting:
      return ((hash<BDD_ID>()(k.i)
               ^ (hash<BDD_ID>()(k.t) << 1)) >> 1)
               ^ (hash<BDD_ID>()(k.e) << 1);
    }
  };

}

namespace mwBDD {

	const BDD_ID Terminal_0 = 0;
	const BDD_ID Terminal_1 = 1;
	/**
	 * @brief BDD Node Structure
	 * @details Structure to store a BDD Node information, include its top variable, BDD ID of the high branch, and BDD ID of the low branch
	 */
	struct BDD_Node {
		/**
		 * BDD ID index of the top variable of this node
		 */
		BDD_ID top_var;

		/**
		 * BDD ID index of the high branch of this node
		 */
		BDD_ID high;

		/**
		 * BDD ID index of the low branch of this node
		 */
		BDD_ID low;

		/**
		 * @brief Equal Operator Overloading
		 * @details Overload Equal Sign Operator to compare two BDD Node
		 *
		 * @param otherNode rvalue BDD Node to compare with the lvalue BDD Node
		 * @return True if two node are equal otherwise false
		 */
		bool operator == (const BDD_Node &otherNode) const {
			return (top_var == otherNode.top_var
					&& high == otherNode.high
					&& low == otherNode.low);
		}
	};

	/**
	 * @brief Class to manage BDD diagrams
	 * @details Manager class which provides APIs to manipulate BDD diagrams. The BDD Diagram will be store in a hash table named Unique Table. All the variable of the BDD Diagram will be store in a hash table name Variable table
	 *
	 */
	class Manager {
	private:
		/**
		 * @brief Find a node in the Unique table and add this node into the table if not exist
		 * @details Find a node in the Unique table and add this node into the table if not exist
		 *
		 * @param BDDNode The BDD Node to be find
		 * @return BDD ID index of this node in the Unique table
		 */
		BDD_ID find_or_add_unique_table(BDD_Node BDDNode);

		/**
		 * @brief Update the Computed table for each computed ITE Operation
		 * @details Update the Computed table for each computed ITE Operation
		 *
		 * @param i The BDD ID index of the root node of the IF function branch
		 * @param t The BDD ID index of the root node of the THEN function branch
		 * @param e The BDD ID index of the root node of the ELSE function branch
		 * @param result The BDD ID index of the root node of the result function
		 */
		void update_computed_table(BDD_ID i, BDD_ID t, BDD_ID e, BDD_ID result );

		/**
		 * @brief Find in the computed table the result of a ITE operation
		 * @details Find in the computed table the result of a ITE operation
		 *
		 * @param i The BDD ID index of the root node of the IF function branch
		 * @param t The BDD ID index of the root node of the THEN function branch
		 * @param e The BDD ID index of the root node of the ELSE function branch
		 * @return The BDD ID index of the root node of the result function in the Unique table of found, otherwise max value of size_t, i.e. (size_t) - 1
		 */
		BDD_ID find_computed_table(BDD_ID i, BDD_ID t, BDD_ID e);

		/**
		 * @brief Check if this BDD ID index does not exist in the Unique table
		 * @details Check if this BDD ID index does not exist in the Unique table
		 *
		 * @param f The BDD ID index need to be checked
		 * @return True if this BDD ID index does not exist, otherwise false
		 */
		bool isValid_BDD_ID(BDD_ID f);
	public:
		/**
		 * Unique Table (Hash Table) contains all nodes of BDD diagrams
		 */
		unordered_map<BDD_ID, BDD_Node> UniqueTable;

		/**
		 * Computed Table (Hash Table) contains all results of computed ITE Operations
		 */
		unordered_map<ITE,BDD_ID> ComputedTable;

		/**
		 * Variable Table (Hash Table) contains all variables of BDD diagrams
		 */
		unordered_map<string, BDD_ID> VariableTable;

		/**
		 * @brief Constructor to initialize a Manager Class
		 * @details Constructor to initially a Manager Object with the terminal nodes (0 and 1)
		 */
		Manager();

		/**
		 * @brief Destructor of the Manager Class
		 * @details Destructor to clear all allocated memory
		 */
		~Manager();

		/**
		 * @brief Create a new variable for the BDD diagram
		 * @details This API will add a new BDD node (top variable is the new variable and the two high and low are terminal nodes) into the Unique table and new variable into Variable Table
		 *
		 * @param label Label of the new variable, e.g. a, b, c, ...
		 * @return The BDD ID index of this new node in the Unique table
		 */
		BDD_ID createVar(const string& label);

		/**
		 * @brief Return the BDD ID of the 1-terminal node
		 * @details Return the BDD ID of the 1-terminal node in the Unique Table of this BDD diagram
		 * @return BDD_ID index of the 1-terminal node in the Unique Table
		 */
		const BDD_ID& True();

		/**
		 * @brief Return the BDD ID of the 0-terminal node
		 * @details Return the BDD ID of the 0-terminal node in the Unique Table of this BDD diagram
		 * @return BDD_ID index of the 1-terminal node in the Unique table
		 */
		const BDD_ID& False();

		/**
		 * @brief Check a BDD node if this node is a Constant
		 * @details A node is a Constant if this node is a terminal node
		 *
		 * @param f BDD ID index of the node in the Unique table need to be checked
		 * @return True if it is a constant/terminal node, otherwise false
		 */
		bool isConstant(const BDD_ID f);

		/**
		 * @brief Check if a BDD node is variable node
		 * @details A BDD node is a variable node if its high and low branches are 1 and 0 terminal node, respectively
		 *
		 * @param f BDD ID index of the node in the Unique table need to be checked
		 * @return True if the node is a variable node, otherwise false
		 */
		bool isVariable(const BDD_ID f);

		/**
		 * @brief Find the top variable of a BDD node
		 * @details Find the BDD ID index of the top variable node of this BDD node
		 *
		 * @param f BDD ID index of the node in the Unique table need to be find its top variable
		 * @return BDD ID index of the top variable node
		 */
		size_t topVar(const BDD_ID f);

		/**
		 * @brief Compute the Shannon co-factor True w.r.t any variable
		 * @details Compute the Shannon co-factor True w.r.t the any variable of a function (represented by a BDD diagram)
		 *
		 * @param f The BDD ID index of the root node of this BDD diagram
		 * @param x The BDD ID index of the variable node used to calculate the Shannon co-factor true
		 *
		 * @return The BDD ID index of the root node of a BDD diagram which is the result of this cofactor
		 */
		BDD_ID coFactorTrue(const BDD_ID f, BDD_ID x);

		/**
		 * @brief Compute the Shannon co-factor False w.r.t any variable
		 * @details Compute the Shannon co-factor False w.r.t the any variable of a function (represented by a BDD diagram)
		 *
		 * @param f The BDD ID index of the root node of this BDD diagram
		 * @param x The BDD ID index of the variable node used to calculate the Shannon co-factor true
		 *
		 * @return The BDD ID index of the root node of a BDD diagram which is the result of this cofactor
		 */
		BDD_ID coFactorFalse(const BDD_ID f, BDD_ID x);

		/**
		 * @brief Compute the Shannon co-factor True w.r.t the top variable
		 * @details Compute the Shannon co-factor True w.r.t the top variable of a function (represented by a BDD diagram)
		 *
		 * @param f The BDD ID index of the root node of the BDD diagram need to be find its co-factor
		 * @return The BDD ID index of the root node of a BDD diagram which is the result of this co-factor
		 */
		BDD_ID coFactorTrue(const BDD_ID f);

		/**
		 * @brief Compute the Shannon co-factor False w.r.t the top variable
		 * @details Compute the Shannon co-factor False w.r.t the top variable of a function (represented by a BDD diagram)
		 *
		 * @param f The BDD ID index of the root node of the BDD diagram need to be find its co-factor
		 * @return The BDD ID index of the root node of a BDD diagram which is the result of this co-factor
		 */
		BDD_ID coFactorFalse(const BDD_ID f);

		/**
		 * @brief Compute ITE operation
		 * @details Calculate ITE operation of three functions (represent by two BDD diagrams)
		 *
		 * @param i The BDD ID index of the root node of the IF branch function
		 * @param t The BDD ID index of the root node of the THEN branch function
		 * @param e The BDD ID index of the root node of the ELSE branch function
		 *
		 * @return The BDD ID index of the root node of the result function
		 */
		BDD_ID ite(const BDD_ID i, const BDD_ID t, const BDD_ID e );

		/**
		 * @brief Compute AND operation
		 * @details Calculate AND operation between two functions (represented by two BDD diagrams)
		 *
		 * @param a The BDD ID index of the root node of the first function
		 * @param b The BDD ID index of the root node of the second function
		 *
		 * @return The BDD ID index of the root node of the result function
		 */
		BDD_ID and2(const BDD_ID a, const BDD_ID b);

		/**
		 * @brief Compute OR operation
		 * @details Calculate OR operation between two functions (represented by two BDD diagrams)
		 *
		 * @param a The BDD ID index of the root node of the first function
		 * @param b The BDD ID index of the root node of the second function
		 *
		 * @return The BDD ID index of the root node of the result function
		 */
		BDD_ID or2(const BDD_ID a, const BDD_ID b);

		/**
		 * @brief Compute XOR operation
		 * @details Calculate XOR operation between two functions (represented by two BDD diagrams)
		 *
		 * @param a The BDD ID index of the root node of the first function
		 * @param b The BDD ID index of the root node of the second function
		 *
		 * @return The BDD ID index of the root node of the result function
		 */
		BDD_ID xor2(const BDD_ID a, const BDD_ID b);

		/**
		 * @brief Compute NOT operation
		 * @details Calculate NOT operation on a function (represented by a BDD diagrams)
		 *
		 * @param a The BDD ID index of the root node of the function
		 *
		 * @return The BDD ID index of the root node of the result function
		 */
		BDD_ID neg(const BDD_ID a);

		/**
		 * @brief Compute NAND operation
		 * @details Calculate NAND operation between two functions (represented by two BDD diagrams)
		 *
		 * @param a The BDD ID index of the root node of the first function
		 * @param b The BDD ID index of the root node of the second function
		 *
		 * @return The BDD ID index of the root node of the result function
		 */
		BDD_ID nand2(const BDD_ID a, const BDD_ID b);

		/**
		 * @brief Compute NOR operation
		 * @details Calculate NOR operation between two functions (represented by two BDD diagrams)
		 *
		 * @param a The BDD ID index of the root node of the first function
		 * @param b The BDD ID index of the root node of the second function
		 *
		 * @return The BDD ID index of the root node of the result function
		 */
		BDD_ID nor2(const BDD_ID a, const BDD_ID b);

		/**
		 * @brief Find the top variable name of a BDD Node
		 * @details Find the top variable name of a BDD Node
		 *
		 * @param root The BDD ID index of the BDD node
		 * @return The name of the top variable, e.g. a, b, c, d ...
		 */
		string getTopVarName(const BDD_ID& root);

		/**
		 * @brief Find all BDD nodes of a BDD Diagram
		 * @details Find all BDD node of a BDD Diagram
		 *
		 * @param root The BDD ID index of the root node of this BDD diagram
		 * @param nodes_of_root A set of indexes of all nodes containing in this BDD diagram
		 */
		void findNodes(const BDD_ID& root, std::set<BDD_ID>& nodes_of_root);

		/**
		 * @brief Find all variables of a BDD Diagram
		 * @details Find all variable of a BDD Diagram
		 *
		 * @param root The BDD ID index of the root node of this BDD diagram
		 * @param vars_of_root A set of indexes of all variables containing in this BDD diagram
		 */
		void findVars(const BDD_ID& root, std::set<BDD_ID>& vars_of_root);

		/**
		 * @brief Create and add a BDD Node for a BDD diagram. TESTING ONLY
		 * @details Manually add a BDD node of a BDD diagram into the Unique table
		 *
		 * @param top_var BDD ID index of the top variable of this node
		 * @param high BDD ID index of the high branch of this node
		 * @param low BDD ID index of the low branch of this node
		 * @param label String label of the top variable of this node
		 */
		void createBDDNode(BDD_ID top_var, BDD_ID high, BDD_ID low, string label);
};

}

#endif /* MANAGE_HPP_ */
