#include "ManagerTest.hpp"

//CppUnit: register suite into the TestFactoryRegistry
CPPUNIT_TEST_SUITE_REGISTRATION(ManagerTest);

//CppUnit: override setUp() to initialize the variables
void ManagerTest::setUp(void) {
	// initialize objects
	miniBDD = new Manager();
}

//CppUnit: override tearDown() to release any permanent resources
//         you allocated in setUp()
void ManagerTest::tearDown(void) {
	// delete objects
	delete miniBDD;
}

/// Method for Unit Test: checks the constructor
void ManagerTest::constructorTest(void) {
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 2);

	CPPUNIT_ASSERT(miniBDD->UniqueTable[0].top_var == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[0].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[0].high == 0);

	CPPUNIT_ASSERT(miniBDD->UniqueTable[1].top_var == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[1].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[1].high == 1);
}

// Method for Unit Test: checks the Create Variable Method
void ManagerTest::createVarTest(void) {
	// create a new variable and check its returned BDD ID
	BDD_ID id = miniBDD->createVar("a");
	CPPUNIT_ASSERT(id == 2);

	// check the new variable's top variable, low and high branch id in the Unique Table
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id].top_var == id);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id].high == 1);

	// recreate that new variable and check if its returned ID is the same
	CPPUNIT_ASSERT(miniBDD->createVar("a") == 2);

	// create a new variable and check if its returned ID is 1 increment
	CPPUNIT_ASSERT(miniBDD->createVar("b") == 2+1);

	// create terminal variable and check if its returned ID is correct
	CPPUNIT_ASSERT(miniBDD->createVar("0") == 0);
	CPPUNIT_ASSERT(miniBDD->createVar("1") == 1);
}

void ManagerTest::TrueTest(void) {
	// check if true() returns 1-terminal node
	CPPUNIT_ASSERT(miniBDD->True() == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[1].top_var == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[1].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[1].high == 1);
}

void ManagerTest::FalseTest(void) {
	// check if false() returns 0-terminal node
	CPPUNIT_ASSERT(miniBDD->False() == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[0].top_var == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[0].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[0].high == 0);
}

void ManagerTest::isContantTest(void) {
	// initialize a test BDD tree
	initUniqTable();

	// assert on terminal nodes
	CPPUNIT_ASSERT(miniBDD->isConstant(0) == true);
	CPPUNIT_ASSERT(miniBDD->isConstant(1) == true);

	// assert on variables
	CPPUNIT_ASSERT(miniBDD->isConstant(2) == false);
	CPPUNIT_ASSERT(miniBDD->isConstant(3) == false);
	CPPUNIT_ASSERT(miniBDD->isConstant(4) == false);
	CPPUNIT_ASSERT(miniBDD->isConstant(5) == false);

	// assert on nodes
	CPPUNIT_ASSERT(miniBDD->isConstant(6) == false);
	CPPUNIT_ASSERT(miniBDD->isConstant(7) == false);
	CPPUNIT_ASSERT(miniBDD->isConstant(8) == false);
	CPPUNIT_ASSERT(miniBDD->isConstant(9) == false);

	// assert throw valid exception
	CPPUNIT_ASSERT_THROW(miniBDD->isVariable(10), Invalid_BDD_ID_Exeption);

}

void ManagerTest::isVariableTest(void) {
	// initialize a test BDD tree
	initUniqTable();

	// assert on terminal nodes
	CPPUNIT_ASSERT(miniBDD->isVariable(0) == false);
	CPPUNIT_ASSERT(miniBDD->isVariable(1) == false);

	// assert on variables
	CPPUNIT_ASSERT(miniBDD->isVariable(2) == true);
	CPPUNIT_ASSERT(miniBDD->isVariable(3) == true);
	CPPUNIT_ASSERT(miniBDD->isVariable(4) == true);
	CPPUNIT_ASSERT(miniBDD->isVariable(5) == true);

	// assert on nodes
	CPPUNIT_ASSERT(miniBDD->isVariable(6) == false);
	CPPUNIT_ASSERT(miniBDD->isVariable(7) == false);
	CPPUNIT_ASSERT(miniBDD->isVariable(8) == false);
	CPPUNIT_ASSERT(miniBDD->isVariable(9) == false);

	// assert throw valid exception
	CPPUNIT_ASSERT_THROW(miniBDD->isVariable(10), Invalid_BDD_ID_Exeption);
}

void ManagerTest::topVarTest(void) {
	// initialize a test BDD tree
	initUniqTable();

	// assert on nodes
	CPPUNIT_ASSERT_EQUAL(size_t(2), miniBDD->topVar(9));
	CPPUNIT_ASSERT_EQUAL(size_t(3), miniBDD->topVar(8));
	CPPUNIT_ASSERT_EQUAL(size_t(4), miniBDD->topVar(7));
	CPPUNIT_ASSERT_EQUAL(size_t(2), miniBDD->topVar(6));
	CPPUNIT_ASSERT_EQUAL(size_t(5), miniBDD->topVar(5));

	// assert throw valid exception
	CPPUNIT_ASSERT_THROW(miniBDD->topVar(10), Invalid_BDD_ID_Exeption);

}

void ManagerTest::coFactorTrueTest2(void) {
	// initialize a test BDD tree
	initUniqTable();

	// check terminal cases
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(0, 2) == 0);
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(1, 2) == 1);
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(7, 2) == 7);
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(7, 3) == 7);
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(7, 4) == 5);

	// test on variable that function contains
	CPPUNIT_ASSERT_EQUAL((BDD_ID)11, miniBDD->coFactorTrue(9,5));
	CPPUNIT_ASSERT(miniBDD->UniqueTable[11].low == 10);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[11].high == 4);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[11].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[10].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[10].high == 4);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[10].top_var == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size()== 12);

	// test on variable that function do not contain
	BDD_ID id = miniBDD->createVar("e");
	CPPUNIT_ASSERT_EQUAL((BDD_ID)9, miniBDD->coFactorTrue(9,id));
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size()== 13);
	CPPUNIT_ASSERT_EQUAL((BDD_ID)9, miniBDD->coFactorTrue(9,20));
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size()== 13);


	// assert on non-existed function
	CPPUNIT_ASSERT_THROW(miniBDD->coFactorTrue(22, 5), Invalid_BDD_ID_Exeption);
}

void ManagerTest::coFactorFalseTest2(void) {
	// initialize a test BDD tree
	initUniqTable();

	// check terminal cases
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(0, 2) == 0);
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(1, 2) == 1);
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(7, 2) == 7);
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(7, 3) == 7);
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(7, 4) == 0);

	// test on variable that function contain
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(9,5)== 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size()== 10);
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(9,3)== 10);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[10].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[10].high == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[10].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size()== 11);

	// test on variable that function do not contain
	BDD_ID id = miniBDD->createVar("e");
	CPPUNIT_ASSERT_EQUAL((BDD_ID)9, miniBDD->coFactorFalse(9,id));\
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size()== 12);
	CPPUNIT_ASSERT_EQUAL((BDD_ID)9, miniBDD->coFactorFalse(9,20));
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size()== 12);

	// assert on non-existed function
	CPPUNIT_ASSERT_THROW(miniBDD->coFactorFalse(22, 5), Invalid_BDD_ID_Exeption);

}

void ManagerTest::coFactorTrueTest1(void) {
	// create BDD: f = (a + b).c.d
	initUniqTable();

	// check coFactor true
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(9) == 7);
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(8) == 7);
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(7) == 5);
	CPPUNIT_ASSERT(miniBDD->coFactorTrue(6) == 1);

	// check invalid exception
	CPPUNIT_ASSERT_THROW(miniBDD->coFactorTrue(22), Invalid_BDD_ID_Exeption);
}

void ManagerTest::coFactorFalseTest1(void) {
	// create BDD: f = (a + b).c.d
	initUniqTable();

	// check coFactor false
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(9) == 8);
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(8) == 0);
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(7) == 0);
	CPPUNIT_ASSERT(miniBDD->coFactorFalse(6) == 3);

	// check invalid exception
	CPPUNIT_ASSERT_THROW(miniBDD->coFactorFalse(22), Invalid_BDD_ID_Exeption);
}

void ManagerTest::iteTest(void) {
	// create variables for checking ITE
	BDD_ID id_a = miniBDD->createVar("a");
	BDD_ID id_b = miniBDD->createVar("b");
	BDD_ID id_c = miniBDD->createVar("c");
	BDD_ID id_d = miniBDD->createVar("d");

	// check if invalid id exception is thrown when invalid id is passed
	CPPUNIT_ASSERT_THROW(miniBDD->ite(10, 11, 12), Invalid_BDD_ID_Exeption);

	// check terminal cases
	CPPUNIT_ASSERT(miniBDD->ite(1, id_a, id_b) == id_a);
	CPPUNIT_ASSERT(miniBDD->ite(0, id_a, id_b) == id_b);
	CPPUNIT_ASSERT(miniBDD->ite(id_c, id_a, id_a) == id_a);
	CPPUNIT_ASSERT(miniBDD->ite(id_c, 1, 0) == id_c);

	// check a + b ITE
	BDD_ID id_or_ab = miniBDD->ite(id_a, 1, id_b);
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_a, id_a, id_b), id_or_ab);
	CPPUNIT_ASSERT(id_or_ab == 6);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].high == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].low == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].top_var == 2);

	// check computed table
	CPPUNIT_ASSERT(id_or_ab == miniBDD->ite(id_a, 1, id_b));

	// check c.d ITE
	BDD_ID id_and_cd = miniBDD->ite(id_c, id_d, 0);
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_c, id_d, id_c), id_and_cd);
	CPPUNIT_ASSERT(id_and_cd == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].high == 5);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].top_var == 4);

	// check (a + b).c.d ITE
	BDD_ID id_f = miniBDD->ite(id_and_cd, id_or_ab, 0);
	CPPUNIT_ASSERT(id_f == 9);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].high == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].low == 8);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].high == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].top_var == 3);

	// check equivalent ite
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, id_or_ab, id_and_cd), miniBDD->ite(id_or_ab, 1, id_and_cd));
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, id_and_cd, id_or_ab), miniBDD->ite(id_or_ab, id_and_cd, 0));
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, id_and_cd, miniBDD->neg(id_or_ab)), miniBDD->ite(id_or_ab, id_and_cd, 1));
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, miniBDD->neg(id_or_ab), id_and_cd), miniBDD->ite(id_or_ab, 0, id_and_cd));
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, 1, id_and_cd), miniBDD->ite(id_and_cd, 1, id_or_ab));
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, id_and_cd, 0), miniBDD->ite(id_and_cd, id_or_ab, 0));
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, id_and_cd, 1), miniBDD->ite(miniBDD->neg(id_and_cd), miniBDD->neg(id_or_ab), 1));
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, 0, id_and_cd), miniBDD->ite(miniBDD->neg(id_and_cd), 0, miniBDD->neg(id_or_ab)));
	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_or_ab, id_and_cd, miniBDD->neg(id_and_cd)), miniBDD->ite(id_and_cd, id_or_ab, miniBDD->neg(id_or_ab)));


	CPPUNIT_ASSERT_EQUAL(miniBDD->ite(id_f, 1, id_or_ab), id_or_ab);
}

void ManagerTest::and2Test(void) {
	// create variables for checking
	BDD_ID id_a = miniBDD->createVar("a");
	BDD_ID id_b = miniBDD->createVar("b");
	BDD_ID id_c = miniBDD->createVar("c");
	BDD_ID id_d = miniBDD->createVar("d");

	// check case if invalid ids are passed
	CPPUNIT_ASSERT_THROW(miniBDD->and2(10, 11), Invalid_BDD_ID_Exeption);

	// check a.b
	BDD_ID id_and_ab = miniBDD->and2(id_a, id_b);
	CPPUNIT_ASSERT(id_and_ab == 6);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].high == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 7);

	// check c.d
	BDD_ID id_and_cd = miniBDD->and2(id_c, id_d);
	CPPUNIT_ASSERT(id_and_cd == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].high == 5);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].top_var == 4);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 8);

	// check a.b.c.d
	BDD_ID id_and_abcd = miniBDD->and2(id_and_ab, id_and_cd);
	CPPUNIT_ASSERT(id_and_abcd == 9);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].high == 8);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].high == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].top_var == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 10);

	// check b.a = a.b
	CPPUNIT_ASSERT_EQUAL(miniBDD->and2(id_b, id_a), id_and_ab);
	// check a.c.c.d.a.b.c.d = a.b.c.d
	CPPUNIT_ASSERT_EQUAL(miniBDD->and2(id_and_abcd, id_and_abcd), id_and_abcd);
	// check c.((b.a).d) = a.b.c.d
	CPPUNIT_ASSERT_EQUAL(miniBDD->and2(id_c, miniBDD->and2(miniBDD->and2(id_b, id_a), id_d)), id_and_abcd);

}

void ManagerTest::or2Test(void) {
	// create variables for checking
	BDD_ID id_a = miniBDD->createVar("a");
	BDD_ID id_b = miniBDD->createVar("b");
	BDD_ID id_c = miniBDD->createVar("c");
	BDD_ID id_d = miniBDD->createVar("d");

	// check case if invalid ids are passed
	CPPUNIT_ASSERT_THROW(miniBDD->or2(10, 11), Invalid_BDD_ID_Exeption);

	// check a + b
	BDD_ID id_or_ab = miniBDD->or2(id_a, id_b);
	CPPUNIT_ASSERT(id_or_ab == 6);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].high == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].low == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 7);

	// check c + d
	BDD_ID id_or_cd = miniBDD->or2(id_c, id_d);
	CPPUNIT_ASSERT(id_or_cd == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].high == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].low == 5);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[7].top_var == 4);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 8);

	// check a + b + c + d
	BDD_ID id_or_abcd = miniBDD->or2(id_or_ab, id_or_cd);
	CPPUNIT_ASSERT(id_or_abcd == 9);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].high == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].low == 8);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[9].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].high == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].low == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[8].top_var == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 10);

	// check b + a = a + b
	CPPUNIT_ASSERT_EQUAL(miniBDD->or2(id_b, id_a), id_or_ab);
	// check c + ((b + a) + d) = a + b + c + d
	CPPUNIT_ASSERT_EQUAL(miniBDD->or2(id_c, miniBDD->or2(miniBDD->or2(id_b, id_a), id_d)), id_or_abcd);
	// check (a + b + c + d) + (a + b + c + d) = a + b + c + d
	CPPUNIT_ASSERT_EQUAL(miniBDD->or2(id_or_abcd, id_or_abcd), id_or_abcd);

}

void ManagerTest::xor2Test(void) {
	// create variables for checking
	BDD_ID id_a = miniBDD->createVar("a");
	BDD_ID id_b = miniBDD->createVar("b");

	// check case if invalid ids are passed
	CPPUNIT_ASSERT_THROW(miniBDD->xor2(10, 11), Invalid_BDD_ID_Exeption);

	// check a XOR b
	BDD_ID id_a_xor_b = miniBDD->xor2(id_a, id_b);
	CPPUNIT_ASSERT(id_a_xor_b == 5);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].low == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].high == 4);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].top_var == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].high == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 6);

	// check a XOR b = ^a.b + a.^b
	BDD_ID id_not_a = miniBDD->neg(id_a);
	BDD_ID id_not_b = miniBDD->neg(id_b);
	BDD_ID id_and_a_not_b = miniBDD->and2(id_a, id_not_b);
	BDD_ID id_and_b_not_a = miniBDD->and2(id_b, id_not_a);
	CPPUNIT_ASSERT_EQUAL(id_a_xor_b, miniBDD->or2(id_and_a_not_b, id_and_b_not_a));
}

void ManagerTest::negTest(void) {
	// create temporal variables
	BDD_ID id_a = miniBDD->createVar("a");
	BDD_ID id_b = miniBDD->createVar("b");

	// check invalid exception
	CPPUNIT_ASSERT_THROW(miniBDD->neg(10), Invalid_BDD_ID_Exeption);

	// check ^a
	BDD_ID id_not_a = miniBDD->neg(id_a);
	CPPUNIT_ASSERT(id_not_a == 4);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_not_a].high == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_not_a].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_not_a].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 5);

	// check ^(a.b)
	BDD_ID id_and_ab = miniBDD->and2(id_a, id_b);
	CPPUNIT_ASSERT(id_and_ab == 5);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_and_ab].high == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_and_ab].low == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_and_ab].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 6);
	BDD_ID id_not_and_ab = miniBDD->neg(id_and_ab);
	CPPUNIT_ASSERT(id_not_and_ab == 7);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_not_and_ab].high == 6);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_not_and_ab].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[id_not_and_ab].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].high == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[6].top_var == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 8);

	// check ^(^(a.b)) = a.b
	CPPUNIT_ASSERT_EQUAL(miniBDD->neg(id_not_and_ab), id_and_ab);

}

void ManagerTest::nand2Test(void) {
	// create temporal variables
	BDD_ID id_a = miniBDD->createVar("a");
	BDD_ID id_b = miniBDD->createVar("b");

	// check invalid exception
	CPPUNIT_ASSERT_THROW(miniBDD->nand2(10, 11), Invalid_BDD_ID_Exeption);

	// check a NAND b
	BDD_ID id_a_nand_b = miniBDD->nand2(id_a, id_b);
	CPPUNIT_ASSERT(id_a_nand_b == 5);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].high == 4);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].top_var == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].high == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 6);

	// check a NAND b = b NAND a
	CPPUNIT_ASSERT_EQUAL(id_a_nand_b, miniBDD->nand2(id_b, id_a));
	// check a NAND b = ^(a AND b)
	CPPUNIT_ASSERT_EQUAL(id_a_nand_b, miniBDD->neg(miniBDD->and2(id_b, id_a)));
	// check a NAND b = ^a + ^b
	CPPUNIT_ASSERT_EQUAL(id_a_nand_b, miniBDD->or2(miniBDD->neg(id_a), miniBDD->neg(id_b)));

}

void ManagerTest::nor2Test(void) {
	// create temporal variable
	BDD_ID id_a = miniBDD->createVar("a");
	BDD_ID id_b = miniBDD->createVar("b");

	// check invalid exception
	CPPUNIT_ASSERT_THROW(miniBDD->nand2(10, 11), Invalid_BDD_ID_Exeption);

	// check a NOR b
	BDD_ID id_a_nor_b = miniBDD->nor2(id_a, id_b);
	CPPUNIT_ASSERT(id_a_nor_b == 5);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].top_var == 2);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].low == 4);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[5].high == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].top_var == 3);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].low == 1);
	CPPUNIT_ASSERT(miniBDD->UniqueTable[4].high == 0);
	CPPUNIT_ASSERT(miniBDD->UniqueTable.size() == 6);

	// check a NOR b = b NOR a
	CPPUNIT_ASSERT_EQUAL(id_a_nor_b, miniBDD->nor2(id_b, id_a));
	// check a NOR b = ^(b + a)
	CPPUNIT_ASSERT_EQUAL(id_a_nor_b, miniBDD->neg(miniBDD->or2(id_b, id_a)));
	// check a NOR b = ^a.^b
	CPPUNIT_ASSERT_EQUAL(id_a_nor_b, miniBDD->and2(miniBDD->neg(id_a), miniBDD->neg(id_b)));

}

void ManagerTest::getTopVarNameTest(void) {
	// create BDD: f = (a + b).c.d
	initUniqTable();

	// check get top variable of f
	string var1 = miniBDD->getTopVarName(9);
	CPPUNIT_ASSERT_EQUAL((string)"a", var1);

	// check get top variable of id 7
	string var2 = miniBDD->getTopVarName(7);
	CPPUNIT_ASSERT_EQUAL((string)"c", var2);

	// check get top variable of terminal nodes
	CPPUNIT_ASSERT_EQUAL((string)"0", miniBDD->getTopVarName(0));
	CPPUNIT_ASSERT_EQUAL((string)"1", miniBDD->getTopVarName(1));

	// check invalid exception
	CPPUNIT_ASSERT_THROW(miniBDD->getTopVarName(10), Invalid_BDD_ID_Exeption);
}

void ManagerTest::findNodesTest(void) {
	// create BDD: f = (a + b).c.d
	initUniqTable();

	set<BDD_ID> nodes_of_root;

	// check terminal cases
	miniBDD->findNodes(0,nodes_of_root);
	CPPUNIT_ASSERT_EQUAL((size_t)1, nodes_of_root.size());
	CPPUNIT_ASSERT(nodes_of_root.find(0) != nodes_of_root.end());
	nodes_of_root.clear();
	miniBDD->findNodes(1,nodes_of_root);
	CPPUNIT_ASSERT_EQUAL((size_t)1, nodes_of_root.size());
	CPPUNIT_ASSERT(nodes_of_root.find(1) != nodes_of_root.end());

	// check set of nodes of function with BDD ID 7
	nodes_of_root.clear();
	miniBDD->findNodes(7,nodes_of_root);
	CPPUNIT_ASSERT_EQUAL((size_t)4, nodes_of_root.size());
	CPPUNIT_ASSERT(nodes_of_root.find(7) != nodes_of_root.end());
	CPPUNIT_ASSERT(nodes_of_root.find(5) != nodes_of_root.end());
	CPPUNIT_ASSERT(nodes_of_root.find(0) != nodes_of_root.end());
	CPPUNIT_ASSERT(nodes_of_root.find(1) != nodes_of_root.end());

	// check set of nodes of function f
	nodes_of_root.clear();
	miniBDD->findNodes(9,nodes_of_root);
	CPPUNIT_ASSERT_EQUAL((size_t)6, nodes_of_root.size());
	CPPUNIT_ASSERT(nodes_of_root.find(9) != nodes_of_root.end());
	CPPUNIT_ASSERT(nodes_of_root.find(8) != nodes_of_root.end());
	CPPUNIT_ASSERT(nodes_of_root.find(7) != nodes_of_root.end());
	CPPUNIT_ASSERT(nodes_of_root.find(5) != nodes_of_root.end());
	CPPUNIT_ASSERT(nodes_of_root.find(0) != nodes_of_root.end());
	CPPUNIT_ASSERT(nodes_of_root.find(1) != nodes_of_root.end());

	// check invalid exception
	nodes_of_root.clear();
	CPPUNIT_ASSERT_THROW(miniBDD->findNodes(10, nodes_of_root), Invalid_BDD_ID_Exeption);
	CPPUNIT_ASSERT_EQUAL((size_t)0, nodes_of_root.size());

}

void ManagerTest::findVarTest(void) {
	// create BDD: f = (a + b).c.d
	initUniqTable();

	set<BDD_ID> vars_of_root;

	// Check terminal cases
	miniBDD->findVars(0,vars_of_root);
	CPPUNIT_ASSERT_EQUAL((size_t)0, vars_of_root.size());
	vars_of_root.clear();
	miniBDD->findVars(1,vars_of_root);
	CPPUNIT_ASSERT_EQUAL((size_t)0, vars_of_root.size());

	// check set of variables of function with BDD ID 7
	vars_of_root.clear();
	miniBDD->findVars(7,vars_of_root);
	CPPUNIT_ASSERT_EQUAL((size_t)2, vars_of_root.size());
	CPPUNIT_ASSERT(vars_of_root.find(4) != vars_of_root.end());
	CPPUNIT_ASSERT(vars_of_root.find(5) != vars_of_root.end());

	// check set of variables of function 9
	vars_of_root.clear();
	miniBDD->findVars(9,vars_of_root);
	CPPUNIT_ASSERT_EQUAL((size_t)4, vars_of_root.size());
	CPPUNIT_ASSERT(vars_of_root.find(5) != vars_of_root.end());
	CPPUNIT_ASSERT(vars_of_root.find(4) != vars_of_root.end());
	CPPUNIT_ASSERT(vars_of_root.find(3) != vars_of_root.end());
	CPPUNIT_ASSERT(vars_of_root.find(2) != vars_of_root.end());

	// check invalid exception
	vars_of_root.clear();
	CPPUNIT_ASSERT_THROW(miniBDD->findVars(10, vars_of_root), Invalid_BDD_ID_Exeption);
	CPPUNIT_ASSERT_EQUAL((size_t)0, vars_of_root.size());


}
/**
 * Create BDD: f = (a + b).c.d
 */
void ManagerTest::initUniqTable(void) {
	miniBDD->createBDDNode(2, 1, 0, "a");
	miniBDD->createBDDNode(3, 1, 0, "b");
	miniBDD->createBDDNode(4, 1, 0, "c");
	miniBDD->createBDDNode(5, 1, 0, "d");
	miniBDD->createBDDNode(2, 1, 3, "or");
	miniBDD->createBDDNode(4, 5, 0, "and");
	miniBDD->createBDDNode(3, 7, 0, "x");
	miniBDD->createBDDNode(2, 7, 8, "f");
}
