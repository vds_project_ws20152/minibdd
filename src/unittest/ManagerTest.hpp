#ifndef MANAGERTEST_HPP
#define MANAGERTEST_HPP

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "../Manager.hpp"

using namespace std;
using namespace CppUnit;
using namespace mwBDD;


class ManagerTest : public TestFixture
{
	//CppUnit: declare the suite, passing the class name to the macro
	CPPUNIT_TEST_SUITE(ManagerTest);

	//CppUnit: declare each test case of the fixture in the suite passing the method name
	CPPUNIT_TEST(constructorTest);
	CPPUNIT_TEST(createVarTest);
	CPPUNIT_TEST(TrueTest);
	CPPUNIT_TEST(FalseTest);
	CPPUNIT_TEST(isContantTest);
	CPPUNIT_TEST(isVariableTest);
	CPPUNIT_TEST(topVarTest);
	CPPUNIT_TEST(coFactorTrueTest2);
	CPPUNIT_TEST(coFactorFalseTest2);
	CPPUNIT_TEST(coFactorTrueTest1);
	CPPUNIT_TEST(coFactorFalseTest1);
	CPPUNIT_TEST(iteTest);
	CPPUNIT_TEST(and2Test);
	CPPUNIT_TEST(or2Test);
	CPPUNIT_TEST(xor2Test);
	CPPUNIT_TEST(negTest);
	CPPUNIT_TEST(nand2Test);
	CPPUNIT_TEST(nor2Test);
	CPPUNIT_TEST(getTopVarNameTest);
	CPPUNIT_TEST(findNodesTest);
	CPPUNIT_TEST(findVarTest);

	//CppUnit: end the suite declaration
	CPPUNIT_TEST_SUITE_END ();

public:
	//CppUnit: methods from the base class overridden in this class
	void setUp(void);
	void tearDown(void);

	// Methods with unit tests
	void constructorTest(void);
	void createVarTest(void);
	void TrueTest(void);
	void FalseTest(void);
	void isContantTest(void);
	void isVariableTest(void);
	void topVarTest(void);
	void coFactorTrueTest2(void);
	void coFactorFalseTest2(void);
	void coFactorTrueTest1(void);
	void coFactorFalseTest1(void);
	void iteTest(void);
	void and2Test(void);
	void or2Test(void);
	void xor2Test(void);
	void negTest(void);
	void nand2Test(void);
	void nor2Test(void);
	void getTopVarNameTest(void);
	void findNodesTest(void);
	void findVarTest(void);
	void initUniqTable(void);

private:

	//CppUnit: a fixture is a known set of objects (variables) that serves as a base for a set of test cases.
	//         Note that the same objects will be used in different test methods.
	//         If the same objects are not used in different methods then no fixture is required
	//         and the methods "setUp" and "tearDown" do not need to be overridden

	// CppUnit: add member variables for each part of the fixture
	Manager* miniBDD;

};

#endif
