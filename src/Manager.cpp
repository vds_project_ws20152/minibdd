#include "Manager.hpp"
#include "limits.h"

using namespace std;
using namespace mwBDD;

Manager::Manager() {

	// Initialize terminal 0;
	BDD_Node Node_0;
	Node_0.top_var = 0;
	Node_0.high = 0;
	Node_0.low = 0;
	UniqueTable.insert(pair<BDD_ID, BDD_Node>(UniqueTable.size(), Node_0));
	VariableTable.insert(pair<string, BDD_ID>("0", Node_0.top_var));

	// Initialize terminal 0;
	BDD_Node Node_1;
	Node_1.top_var = 1;
	Node_1.high = 1;
	Node_1.low = 1;
	UniqueTable.insert(pair<BDD_ID, BDD_Node>(UniqueTable.size(), Node_1));
	VariableTable.insert(pair<string, BDD_ID>("1", Node_1.top_var));

}

Manager::~Manager() {

}

BDD_ID Manager::createVar(const string& label) {

	if (VariableTable.find(label) == VariableTable.end()) {
		// New variable is created;
		BDD_Node newnode;
		newnode.top_var = UniqueTable.size();
		newnode.high = Terminal_1;
		newnode.low = Terminal_0;
		UniqueTable.insert(pair<BDD_ID, BDD_Node>(UniqueTable.size(), newnode));
		VariableTable.insert(pair<string, BDD_ID>(label, VariableTable.size()));
		return newnode.top_var;
	} else {
		// The variable has already been created;
		return VariableTable[label];
	}
}

const BDD_ID& Manager::True() {
	return Terminal_1;
}

const BDD_ID& Manager::False() {
	return Terminal_0;
}

bool Manager::isConstant(const BDD_ID f) {
	if (isValid_BDD_ID(f) == false) {
		throw Invalid_BDD_ID_Exeption();   // throw exception if invalid BDD ID
	} else {
		if ((f == True()) | (f == False())) {
			return true;                     // TRUE if terminal nodes
		} else {
			return false;                    // otherwise, FALSE
		}
	}
}

bool Manager::isVariable(const BDD_ID f) {
	if (isValid_BDD_ID(f) == false) {
		throw Invalid_BDD_ID_Exeption();    // throw exception if invalid BDD ID
	} else {
		if ((UniqueTable[f].high == True())
				&& (UniqueTable[f].low == False())) {
			return true;       // TRUE if low and high branch are terminal nodes
		} else {
			return false;                               // otherwise, FALSE
		}
	}
}

size_t Manager::topVar(const BDD_ID f) {
	if (isValid_BDD_ID(f) == false) {
		throw Invalid_BDD_ID_Exeption();    // throw exception if invalid BDD ID
	} else {
		return UniqueTable[f].top_var;               // return top_var member ID
	}
}

BDD_ID Manager::coFactorTrue(const BDD_ID f, BDD_ID x) {
	if (isValid_BDD_ID(f) == false) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		if (isConstant(f)) {
			return f;
		} else if (topVar(f) > x) {
			return f;
		} else if (topVar(f) == x) {
			return coFactorTrue(f);
		} else {
			BDD_Node Node_temp;
			Node_temp.top_var = topVar(f);
			Node_temp.high = coFactorTrue(coFactorTrue(f), x);
			Node_temp.low = coFactorTrue(coFactorFalse(f), x);

			return ite(Node_temp.top_var, Node_temp.high, Node_temp.low);
		}
	}
}

BDD_ID Manager::coFactorFalse(const BDD_ID f, BDD_ID x) {
	if (isValid_BDD_ID(f) == false) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		if (isConstant(f)) {
			return f;
		} else if (topVar(f) > x) {
			return f;
		} else if (topVar(f) == x) {
			return coFactorFalse(f);
		} else {
			BDD_Node Node_temp;
			Node_temp.top_var = topVar(f);
			Node_temp.high = coFactorFalse(coFactorTrue(f), x);
			Node_temp.low = coFactorFalse(coFactorFalse(f), x);

			return ite(Node_temp.top_var, Node_temp.high, Node_temp.low);
		}
	}
}

BDD_ID Manager::coFactorTrue(const BDD_ID f) {
	if (isValid_BDD_ID(f) == false) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		return UniqueTable[f].high;
	}
}

BDD_ID Manager::coFactorFalse(const BDD_ID f) {

	if (isValid_BDD_ID(f) == false) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		return UniqueTable[f].low;
	}
}

BDD_ID Manager::ite(const BDD_ID i, const BDD_ID t, const BDD_ID e) {
	if ((isValid_BDD_ID(i) == false) | (isValid_BDD_ID(t) == false)
			| (isValid_BDD_ID(e) == false)) {
		throw Invalid_BDD_ID_Exeption();  // throw exception if invalid BDD ID
	} else {
		// Base case
		if (i == True()) {
			return t;
		} else if (i == False()) {
			return e;
		} else if (t == e) {
			return t;
		} else if (t == True() && e == False()) {
			return i;
		} else {
			// find in computed table
			BDD_ID found = find_computed_table(i, t, e);
			if (found != ((size_t) -1)) {
				return found;
			} else {
				// General cases
				// find top variable (i,t,e)
				BDD_ID top_variable = topVar(i);
				if ((topVar(t) < top_variable) && (!isConstant(t))) {
					top_variable = topVar(t);
				}
				if ((topVar(e) < top_variable) && (!isConstant(e))) {
					top_variable = topVar(e);
				}

				BDD_Node Node_temp;
				Node_temp.top_var = top_variable;
				Node_temp.high = ite(coFactorTrue(i, top_variable),
						coFactorTrue(t, top_variable),
						coFactorTrue(e, top_variable));
				Node_temp.low = ite(coFactorFalse(i, top_variable),
						coFactorFalse(t, top_variable),
						coFactorFalse(e, top_variable));

				if (Node_temp.high == Node_temp.low) {
					return Node_temp.high;
				}
				BDD_ID r = find_or_add_unique_table(Node_temp);
				update_computed_table(i, t, e, r);
				return r;
			}
		}
	}
}

BDD_ID Manager::and2(const BDD_ID a, const BDD_ID b) {

	if ((isValid_BDD_ID(a) == false) | (isValid_BDD_ID(b) == false)) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		return ite(a, b, 0);
	}
}

BDD_ID Manager::or2(const BDD_ID a, const BDD_ID b) {

	if ((isValid_BDD_ID(a) == false) | (isValid_BDD_ID(b) == false)) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		return ite(a, 1, b);
	}
}

BDD_ID Manager::xor2(const BDD_ID a, const BDD_ID b) {

	if ((isValid_BDD_ID(a) == false) | (isValid_BDD_ID(b) == false)) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		return ite(a, neg(b), b);
	}
}

BDD_ID Manager::neg(const BDD_ID a) {

	if (isValid_BDD_ID(a) == false) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		return ite(a, 0, 1);
	}
}

BDD_ID Manager::nand2(const BDD_ID a, const BDD_ID b) {

	if ((isValid_BDD_ID(a) == false) | (isValid_BDD_ID(b) == false)) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		return ite(a, neg(b), 1);
	}
}

BDD_ID Manager::nor2(const BDD_ID a, const BDD_ID b) {

	if ((isValid_BDD_ID(a) == false) | (isValid_BDD_ID(b) == false)) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		return ite(a, 0, neg(b));
	}
}

string Manager::getTopVarName(const BDD_ID& root) {
	if (isValid_BDD_ID(root) == false) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		for (auto it = VariableTable.begin(); it != VariableTable.end(); ++it) {
			if (it->second == topVar(root)) {
				return it->first;
			}
		}
		return NULL;
	}
}

void Manager::findNodes(const BDD_ID& root, std::set<BDD_ID>& nodes_of_root) {
	if (isValid_BDD_ID(root) == false) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		if ((root == True()) | (root == False())) {
			nodes_of_root.insert(root);
			return;
		} else {
			nodes_of_root.insert(root);
			findNodes(coFactorTrue(root), nodes_of_root);
			findNodes(coFactorFalse(root), nodes_of_root);
			return;
		}
	}
}

void Manager::findVars(const BDD_ID& root, std::set<BDD_ID>& vars_of_root) {
	if (isValid_BDD_ID(root) == false) {
		throw Invalid_BDD_ID_Exeption();
	} else {
		if ((root == True()) | (root == False())) {
			return;
		} else {
			// Insert the variable if it does not exist in the set
			if (vars_of_root.find(root) == vars_of_root.end()) {
				vars_of_root.insert(topVar(root));
			}
			findVars(coFactorTrue(root), vars_of_root);
			findVars(coFactorFalse(root), vars_of_root);
			return;
		}
	}
}

void Manager::createBDDNode(BDD_ID top_var, BDD_ID high, BDD_ID low,
		string label) {
	BDD_Node newnode;
	newnode.top_var = top_var;
	newnode.high = high;
	newnode.low = low;

	UniqueTable.insert(pair<BDD_ID, BDD_Node>(UniqueTable.size(), newnode));
	if ((high == 1) && (low == 0)) {
		VariableTable.insert(pair<string, BDD_ID>(label, VariableTable.size()));
	}
}

BDD_ID Manager::find_or_add_unique_table(BDD_Node BDDNode) {
	// BDD Node has already been in the uniqueTable
	for (auto it = UniqueTable.begin(); it != UniqueTable.end(); ++it) {
		if (it->second == BDDNode) {
			return it->first;
		}
	}

	// Insert a new BDD Node
	UniqueTable.insert(pair<BDD_ID, BDD_Node>(UniqueTable.size(), BDDNode));
	return UniqueTable.size() - 1;
}

BDD_ID Manager::find_computed_table(BDD_ID i, BDD_ID t, BDD_ID e) {
	ITE new_ite;
	new_ite.i = i;
	new_ite.t = t;
	new_ite.e = e;

	auto got = ComputedTable.find(new_ite);
	if (got == ComputedTable.end())
		return (size_t) -1; // Can not find the ite node in ComputedTable
	else
		return got->second;
}

void Manager::update_computed_table(BDD_ID i, BDD_ID t, BDD_ID e,
		BDD_ID result) {
	ITE new_ite;
	new_ite.i = i;
	new_ite.t = t;
	new_ite.e = e;
	ComputedTable.insert(pair<ITE, BDD_ID>(new_ite, result));
}

bool Manager::isValid_BDD_ID(BDD_ID f) {
	if (f < UniqueTable.size()) {
		return true;
	} else
		return false;
}
