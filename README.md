# 1. DESCRIPTION
In this project, we implement a pico [BDD](https://en.wikipedia.org/wiki/Binary_decision_diagram) (Binary Decision Diagram) package in C++. The package provides standard interfaces to manipulate ROBDDs as they were introduced in [Verification of Digital Systems](https://www.eit.uni-kl.de/en/eis/teaching/85-560/) lecture.

# 2. INSTALLATION
Clone the source codes from bitbucket.org using git clone command:
```
git clone https://<username>@bitbucket.org/vds_project_ws20152/minibdd.git
```
NOTE: Replace ```<username>``` with your username on bitbucket.org

# 3. COMPILING
* Inside the cloned directory, run the following command to compile the source code into a binary library:  
```scons```
* To clean the compiled binary library, types:  
```scons -c```

# 4. API DOCUMENTATION
* To generate the API documentation for the package, navigate into the doc folder, then type:  
```doxygen doxyConfigEx```
* The document can be view by open the **index.html** file (```./doxygen/html/index.html```) in a web browser

# 5. RUNNING TESTS
Only Unit testcases are implemented, so to run the testcases, type the following command from the root cloned directory:  
```scons test```

# 6. DIRECTORY STRUCTURE
* ```./src```  
Source codes of implementation of the library
* ```./src/unittest```  
Source codes of implementation of the unit testcases
* ```./doc```  
Script to generate/update documentation for the library
* ```./test```  
Source codes of main program to run the unit testcases

# 7. CONTRIBUTORS
*  [Anh Quoc Tran](mailto:tranan@rhrk.uni-kl.de)
*  [Cong Thanh Tran](mailto:trancong@rhrk.uni-kl.de)
